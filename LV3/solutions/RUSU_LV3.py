import re
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg
import pandas as pd 

import urllib
import xml.etree.ElementTree as ET


# ----------
# 1. Zadatak
# ----------

mtcars = pd.read_csv('../resources/mtcars.csv')

print("--------------------------------------------")
print("1. Kojih 5 automobila ima najveću potrošnju?")
print(mtcars.sort_values(by=['mpg'], ascending = True).head(5))

print("--------------------------------------------------------------")
print("2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?")
CarCyl8 = mtcars[mtcars.cyl == 8]
print(CarCyl8.sort_values(by=['mpg'], ascending = True).tail(3))

print("---------------------------------------------------------")
print("3. Kolika je srednja potrošnja automobila sa 6 cilindara?")
CarCyl6 = mtcars[mtcars.cyl == 6] 
CarCyl6_avgConsumption = CarCyl6["mpg"].mean()
print(CarCyl6_avgConsumption)

print("-----------------------------------------------------------------------------------")
print("4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?")
CarCyl4_mass2000to2200 = mtcars[(mtcars.cyl == 4) & (mtcars.wt>2) & (mtcars.wt<2.2)]
CarCyl4_mass2000to2200_avgConsumption = CarCyl4_mass2000to2200["mpg"].mean()
print(CarCyl4_mass2000to2200_avgConsumption)

print("-----------------------------------------------------------------------------------------")
print("5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?")
gearboxType = mtcars.groupby('am').car
print(gearboxType.count())

print("----------------------------------------------------------------------------------")
print("6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?")
Cars_automaticGearbox_over100hp = mtcars[(mtcars.am == 0) & (mtcars.hp>100)]
print(Cars_automaticGearbox_over100hp["car"].count())

print("--------------------------------------------------")
print("7. Kolika je masa svakog automobila u kilogramima?")
pound2kg = 0.45359    # 1 pound = 0.45359 kg
CarIndex = 0
for wt in mtcars["wt"]:   
    wt_kg_value = (wt * 1000) * pound2kg         
    print(str(CarIndex) + ". " + mtcars.car[CarIndex] + ": %.3f kg" % (wt_kg_value))
    CarIndex = CarIndex + 1 
    

# ----------
# 2. Zadatak
# ----------


# mtcars = pd.read_csv('../resources/mtcars.csv')

# # 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
# CarCyl4 = mtcars[mtcars.cyl == 4]
# CarCyl6 = mtcars[mtcars.cyl == 6]
# CarCyl8 = mtcars[mtcars.cyl == 8]

# CarCyl4_indexes = np.arange(0, len(CarCyl4), 1)
# CarCyl6_indexes = np.arange(0, len(CarCyl6), 1) 
# CarCyl8_indexes = np.arange(0, len(CarCyl8), 1) 

# widthBetweenColumns = 0.3

# plt.figure()

# plt.bar(CarCyl4_indexes, CarCyl4["mpg"], widthBetweenColumns, color="r")
# plt.bar(CarCyl6_indexes + widthBetweenColumns, CarCyl6["mpg"], widthBetweenColumns, color="g")
# plt.bar(CarCyl8_indexes + 2*widthBetweenColumns, CarCyl8["mpg"], widthBetweenColumns, color="b")
# plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
# plt.xlabel('auto')
# plt.ylabel('mpg')
# plt.legend(['4 cilindra','6 cilindara','8 cilindara'], loc=1)
# plt.grid(axis='y', linestyle='--')

# # 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
# CarWeight4=[]
# CarWeight6=[]
# CarWeight8=[]

# for i in CarCyl4["wt"]:
#     CarWeight4.append(i) 

# for i in CarCyl6["wt"]:
#     CarWeight6.append(i)  

# for i in CarCyl8["wt"]:
#     CarWeight8.append(i)  

# plt.figure()
# plt.boxplot([CarWeight4, CarWeight6, CarWeight8], positions = [4,6,8]) 
# plt.title("Tezina automobila s 4, 6 i 8 cilindara")
# plt.xlabel('Broj klipova')
# plt.ylabel('Tezina wt')
# plt.grid(axis='y',linestyle='--')

# # 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
# # potrošnju od automobila s automatskim mjenjačem?

# automaticCars = mtcars[(mtcars.am == 0)]
# CarConsumption_automatic=[]
# for i in automaticCars["mpg"]:
#     CarConsumption_automatic.append(i)
    
# manualCars = mtcars[(mtcars.am == 1)]
# CarConsumption_manual=[]
# for i in manualCars["mpg"]:
#     CarConsumption_manual.append(i)

# plt.figure()
# plt.boxplot([CarConsumption_manual, CarConsumption_automatic], positions = [0,1])
# plt.title("Usporedba potrosnja automobila s rucnim, odnosno automatskim mjenjacem")
# plt.ylabel('miles per gallon')
# plt.xlabel('automatski mjenjac                         rucni mjenjac')
# plt.grid(axis='y',linestyle='--')

# # 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
# # mjenjačem. 
# AutomaticCar_acceleration=[]
# for i in automaticCars["qsec"]:  
#     AutomaticCar_acceleration.append(i)
    
# AutomaticCar_power=[]  
# for i in automaticCars["hp"]:  
#     AutomaticCar_power.append(i)
    
# ManualCar_acceleration=[]    
# for i in manualCars["qsec"]:  
#     ManualCar_acceleration.append(i)
    
# ManualCar_power=[]    
# for i in manualCars["hp"]:  
#     ManualCar_power.append(i)

# plt.figure()
# plt.scatter(AutomaticCar_acceleration, AutomaticCar_power, marker='+')  
# plt.scatter(ManualCar_acceleration, ManualCar_power, marker='^', facecolors='none', edgecolors='r')
# plt.title("Odnos ubrzanja i snage automobila s rucnim u odnosu na automatski mjenjac")
# plt.ylabel('Snaga - hp')
# plt.xlabel('Ubrzanje - qsec')
# plt.legend(["Automatski mjenjac","Rucni mjenjac"])
# plt.grid(linestyle='--')


# ----------
# 3. Zadatak
# ----------


# # 1. Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek.
# url = "http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=02.01.2017&vrijemeDo=01.01.2018"

# airQualityHR = urllib.request.urlopen(url).read()
# root = ET.fromstring(airQualityHR)
# print(root)

# df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

# i = 0
# while True:
    
#     try:
#         obj = root.getchildren()[i].getchildren()
#     except:
#         break
    
#     row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
#     row_s = pd.Series(row)
#     row_s.name = i
#     df = df.append(row_s)
#     df.mjerenje[i] = float(df.mjerenje[i])
#     i = i + 1

# df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
# df.plot(y='mjerenje', x='vrijeme');

# # add date month and day designator
# df['month'] = pd.DatetimeIndex(df['vrijeme']).month 
# df['dayOfweek'] = pd.DatetimeIndex(df['vrijeme']).dayofweek

# # 2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
# topPM10values = df.sort_values(by=['mjerenje'], ascending=False)
# print("\nTri datuma kad je koncentracija PM10 u 2017. bila najveca: ")
# print(topPM10values['vrijeme'].head(3))

# # 3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
# index = np.arange(1,13,1)
# noDataDaysCount=[]
# for i in range(1,13):
#     month = df[df.month==i]
#     if (i==2):
#         noDataDaysCount.append(28-len(month)) 
#     elif (i==4) or (i==6) or(i==9) or(i==11):
#         noDataDaysCount.append(30-len(month)) 
#     else:
#         noDataDaysCount.append(31-len(month))

# plt.figure()
# plt.bar(index, noDataDaysCount)
# plt.title("Broj izostalih vrijednosti tijekom mjeseci")
# plt.xlabel("mjesec")
# plt.ylabel("broj dana bez unosa mjerenja")
# plt.grid(linestyle="--")

# # 4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
# JuneValues=[]
# NovemberValues=[]
# June=df[df.month==6]
# November=df[df.month==11]

# for i in June["mjerenje"]:
#     JuneValues.append(i)    
    
# for i in November["mjerenje"]:   
#     NovemberValues.append(i)   
    
# plt.figure()
# plt.boxplot([JuneValues, NovemberValues], positions = [6,11]) 
# plt.title("Koncentracija PM10 u lipnju i studenom")
# plt.xlabel("Mjesec")
# plt.ylabel("Koncentracija")
# plt.grid(axis='y',linestyle="-")

# # 5. Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda.
# WorkdayValues=[]
# WeekendValues=[]
# workdays_PM10=df[(df.dayOfweek==0)|(df.dayOfweek==1)|(df.dayOfweek==2)|(df.dayOfweek==3)|(df.dayOfweek==4)]
# weekends_PM10=df[(df.dayOfweek==5)|(df.dayOfweek==6)]
        
# for i in workdays_PM10["mjerenje"]:
#     WorkdayValues.append(i)
# for i in weekends_PM10["mjerenje"]:
#     WeekendValues.append(i)

# bins=25         
# plt.figure()
# plt.hist([WorkdayValues, WeekendValues], bins, color=['blue','red'])
# plt.title("Usporedba distribucije PM10 čestica tijekom radnih dana i vikenda")
# plt.xlabel("Koncentracija tijekom tjedna")
# plt.ylabel("Broj čestica PM10")
# plt.legend(["Radni tjedan","Vikend"], loc=1)
