from sklearn import datasets 
import scipy as sp
import numpy as np 
import sklearn.cluster as cluster
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy.cluster.hierarchy import dendrogram, linkage

print("----- 1. Zadatak --------------------------------------------------")

# def generate_data(n_samples, flagc): 
 
#     if flagc == 1: 
#         random_state = 365 
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) 
 
#     elif flagc == 2: 
#         random_state = 148 
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) 
#         transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]] 
#         X = np.dot(X, transformation) 
 
#     elif flagc == 3: 
#         random_state = 148 
#         X, y = datasets.make_blobs(n_samples=n_samples, 
#         cluster_std=[1.0, 2.5, 0.5, 3.0], 
#         random_state=random_state) 
#     elif flagc == 4: 
#         X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05) 
 
#     elif flagc == 5: 
#         X, y = datasets.make_moons(n_samples=n_samples, noise=.05) 
     
#     else: 
#         X = [] 
 
#     return X 

# X = generate_data(500, 5)
# kMeans = cluster.KMeans(n_clusters = 5)
# predict = kMeans.fit_predict(X)

# for i in range(5):
#     xColor = []
#     yColor = []
#     for j in range(len(predict)):
#         xColor.append(X[j,0])
#         yColor.append(X[j,1])
        
#     plt.scatter(xColor, yColor)


# plt.scatter(kMeans.cluster_centers_[:,0], kMeans.cluster_centers_[:,1], color = 'blue') 
# plt.show()

print("----- 2. Zadatak --------------------------------------------------")

# def generate_data(n_samples, flagc):
    
#     if flagc == 1:
#         random_state = 365
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
#     elif flagc == 2:
#         random_state = 148
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
#         transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
#         X = np.dot(X, transformation)
        
#     elif flagc == 3:
#         random_state = 148
#         X, y = datasets.make_blobs(n_samples=n_samples,
#                           cluster_std=[1.0, 2.5, 0.5, 3.0],
#                           random_state=random_state)

#     elif flagc == 4:
#         X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
#     elif flagc == 5:
#         X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
#     else:
#         X = []
        
#     return X


# X = generate_data(500, 5)

# kMeans = cluster.KMeans(n_clusters = 5)

# predict = kMeans.fit_predict(X)


# for i in range(5):
#     firstColor = []
#     secondColor = []
#     for j in range(len(predict)):
#         firstColor.append(X[j,0])
#         secondColor.append(X[j,1])
        
#     plt.scatter(firstColor, secondColor)


# listRange = list(range(1,20))
# critFunction = []
# for N_clusters in listRange:
#     clusterK = cluster.KMeans(n_clusters = N_clusters)
#     predicted = clusterK.fit_predict(X)
#     critFunction.append(clusterK.inertia_)

# plt.bar(x = listRange, height = critFunction, color = "dodgerblue")
# plt.plot(listRange,critFunction, c = "red")
# plt.xlabel("Cluster number")
# plt.ylabel("Criterial function")
# plt.xticks(listRange)
# plt.show()


print("----- 3. Zadatak --------------------------------------------------")

# def generate_data(n_samples, flagc):

#     if flagc == 1:
#         random_state = 365
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    
#     elif flagc == 2:
#         random_state = 148
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
#         transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
#         X = np.dot(X, transformation)
    
#     elif flagc == 3:
#         random_state = 148
#         X, y = datasets.make_blobs(n_samples=n_samples, centers = 4,
#         cluster_std=[1.0, 2.5, 0.5, 3.0],
#         random_state=random_state)
    
#     elif flagc == 4:
#         X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
#     elif flagc == 5:
#         X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
#     else:
#         X = []
    
#     return X

# X = generate_data(500, 5) #potrebno promijeniti drugi parametar za promjenu dataseta
# for method in ['single', 'complete', 'average', 'weighted','ward']:
#     Z = linkage(X, method)
#     plt.figure()
#     plt.title('Dendrogram za skup podataka 1, method = %s' %method)
#     dendrogram(Z)


print("----- 4. Zadatak --------------------------------------------------")

# imageNew = mpimg.imread('../resources/example_grayscale.png')
    
# X = imageNew.reshape((-1, 1))

# plt.figure()
# plt.title('Originalna slika')
# plt.imshow(imageNew,  cmap='gray')

# for clusterCount in range(2, 11):
#     k_means = cluster.KMeans(n_clusters=clusterCount, n_init=1)
#     k_means.fit(X) 
#     values = k_means.cluster_centers_.squeeze()
#     labels = k_means.labels_
#     imageNew_compressed = np.choose(labels, values)
#     imageNew_compressed.shape = imageNew.shape
    
    
#     plt.figure()
#     plt.title('Slika komprimirana u %d clustera' %clusterCount)
#     plt.imshow(imageNew_compressed,  cmap='gray')


print("----- 5. Zadatak --------------------------------------------------")

# imageNew = mpimg.imread('../resources/example.png')

# try:
#     face = sp.face(gray=True)
# except AttributeError:
#     from scipy import misc
#     face = misc.face(gray=True)
    
# X = face.reshape((-1, 1)) 
# k_means = cluster.KMeans(n_clusters=5,n_init=1)
# k_means.fit(X) 
# values = k_means.cluster_centers_.squeeze()
# labels = k_means.labels_
# face_compressed = np.choose(labels, values)
# face_compressed.shape = face.shape

# plt.figure(1)
# plt.imshow(face,  cmap='gray')

# plt.figure(2)
# plt.imshow(face_compressed,  cmap='gray')
# plt.show


print("----- 6. Zadatak --------------------------------------------------")


# def generate_data(n_samples, flagc):

#     if flagc == 1:
#         random_state = 365
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    
#     elif flagc == 2:
#         random_state = 148
#         X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
#         transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
#         X = np.dot(X, transformation)
    
#     elif flagc == 3:
#         random_state = 148
#         X, y = datasets.make_blobs(n_samples=n_samples, centers = 4,
#         cluster_std=[1.0, 2.5, 0.5, 3.0],
#         random_state=random_state)
    
#     elif flagc == 4:
#         X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
#     elif flagc == 5:
#         X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
#     else:
#         X = []
    
#     return X

# def getDistanceMatrix(x,y):
#     distances = np.empty((x.shape[0], y.shape[0]))
#     for i in range(y.shape[0]):
#         for j in range(x.shape[0]):
#             distances[j,i] = np.sqrt(np.sum((y[i,:] - x[j,:])**2))
#     return distances

# def kMeans(X, n_clusters):
#     centers = X[np.random.choice(X.shape[0], size=n_clusters, replace=False)]
#     centers_list = [centers.copy()]
#     labels = np.empty(X.shape[0], dtype=int)
    
#     for iteration in range(100):
#         distances = getDistanceMatrix(X, centers)
#         for i in range(labels.shape[0]):
#             labels[i] = np.argmin(distances[i,:])
#         for i in range(n_clusters):
#             centers[i] = np.mean(X[labels==i], axis=0)
#         centers_list.append(centers.copy())
#     return labels, centers_list

# cluster_counts = [3, 3, 4, 2, 2]

# for i in range(5):      
#     X = generate_data(500, i+1)
#     labels, centers = kMeans(X, cluster_counts[i])
    
#     plt.figure()
#     plt.scatter(X[:,0], X[:,1], c = labels)
    
#     for center in centers:
#         plt.scatter(center[:,0], center[:,1], c = 'r', marker = '*')
        
#     for j in range(len(centers) - 1):
#         for k in range(cluster_counts[i]):
#             plt.plot([centers[j][k][0], centers[j+1][k][0]], [centers[j][k][1], centers[j+1][k][1]], c = 'k')
