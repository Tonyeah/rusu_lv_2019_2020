import numpy as np
import matplotlib.pyplot as plt 

import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

from sklearn.datasets import load_boston
from itertools import cycle


print('\n----- 1. Zadatak -------------------------------------------')


# def non_func(x): 
#     y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067) 
#     return y
 
# def add_noise(y): 
#     np.random.seed(14) 
#     varNoise = np.max(y) - np.min(y) 
#     y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y)) 
#     return y_noisy 

# x = np.linspace(1,10,100) 
# y_true = non_func(x) 
# y_measured = add_noise(y_true) 

# plt.figure(1) 
# plt.plot(x,y_measured,'ok',label='mjereno') 
# plt.plot(x,y_true,label='stvarno') 
# plt.xlabel('x') 
# plt.ylabel('y') 
# plt.legend(loc = 4) 

# np.random.seed(12) 
# indeksi = np.random.permutation(len(x)) 
# indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] 
# indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)] 

# x = x[:, np.newaxis] 
# y_measured = y_measured[:, np.newaxis] 

# xtrain = x[indeksi_train] 
# ytrain = y_measured[indeksi_train] 

# xtest = x[indeksi_test] 
# ytest = y_measured[indeksi_test] 

# plt.figure(2) 
# plt.plot(xtrain,ytrain,'ob',label='train') 
# plt.plot(xtest,ytest,'or',label='test') 
# plt.xlabel('x') 
# plt.ylabel('y') 
# plt.legend(loc = 4) 

# linearModel = lm.LinearRegression() 
# linearModel.fit(xtrain,ytrain) 

# print('Model je oblika y_hat = Theta0 + Theta1 * x') 
# print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x') 

# ytest_p = linearModel.predict(xtest) 
# MSE_test = mean_squared_error(ytest, ytest_p) 

# plt.figure(3) 
# plt.plot(xtest,ytest_p,'og',label='predicted') 
# plt.plot(xtest,ytest,'or',label='test') 
# plt.legend(loc = 4) 

# x_pravac = np.array([1,10]) 
# x_pravac = x_pravac[:, np.newaxis] 
# y_pravac = linearModel.predict(x_pravac) 
# plt.plot(x_pravac, y_pravac) 

# plt.show()


print('\n----- 2. Zadatak -------------------------------------------')

# def non_func(x):
#   y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
#   return y

# def add_noise(y):
#   np.random.seed(14)
#   varNoise = np.max(y) - np.min(y)
#   y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
#   return y_noisy

# x = np.linspace(1,10,100)
# y_true = non_func(x)
# y_measured = add_noise(y_true)

# np.random.seed(12)
# indeksi = np.random.permutation(len(x))
# indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]

# x = x[:, np.newaxis]
# x = np.append(np.ones((len(x), 1)), x, axis = 1)
# y_measured = y_measured[:, np.newaxis] 

# xtrain = x[indeksi_train]
# ytrain = y_measured[indeksi_train]

# theta = np.linalg.inv(np.transpose(xtrain) @ xtrain) @ np.transpose(xtrain) @ ytrain
# print(theta) # Isti su parametri theta kao y_hat u 1. zadatku



print('\n----- 3. Zadatak -------------------------------------------')

# def non_func(x):
#  	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
#  	return y

# def add_noise(y):
#     np.random.seed(14)
#     varNoise = np.max(y) - np.min(y)
#     y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
#     return y_noisy

# x = np.linspace(1,10,100)
# y_true = non_func(x)
# y_measured = add_noise(y_true)

# np.random.seed(12)
# indeksi = np.random.permutation(len(x))
# indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]

# x = x[:, np.newaxis]
# x = np.append(np.ones((len(x), 1)), x, axis = 1)
# y_measured = y_measured[:, np.newaxis] 

# xtrain = x[indeksi_train]
# ytrain = y_measured[indeksi_train]

# theta = np.zeros((2,1))
# alphas = [0.001, 0.01, 0.05, 0.08]

# for alpha in alphas:
#     criterias = []
#     for itr in range(500):
#         sume = np.zeros((1,2))
#         for i in range(xtrain.shape[0]):
#             sume += ((xtrain[i] @ theta - ytrain[i]) * xtrain[i])
#         sume /= xtrain.shape[0]
#         theta -= alpha * sume.transpose()
#         criterias.append(1/(2*len(xtrain)) * np.sum((xtrain @ theta - ytrain) ** 2))
        
#     print(criterias[0])
#     print('Stopa učenja = %.2f' %alpha)
#     print(theta)
    
#     plt.plot(range(len(criterias)), criterias, label = 'alpha = ' + str(alpha))

# plt.ylim((0.5, 1))
# plt.xlabel('Iteracija')
# plt.ylabel('Vrijednost kriterijske funkcije')
# plt.legend()

# plt.show()


print('\n----- 4. Zadatak -------------------------------------------')

# def non_func(x):
#     y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
#     return y
 
# def add_noise(y):
#     np.random.seed(14)
#     varNoise = np.max(y) - np.min(y)
#     y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
#     return y_noisy
 
# x = np.linspace(1,10,100)
# y_true = non_func(x)
# y_measured = add_noise(y_true)

# x = x[:, np.newaxis]
# y_measured = y_measured[:, np.newaxis] 

# # make polynomial features
# poly = PolynomialFeatures(degree=15)
# xnew = poly.fit_transform(x) 

# np.random.seed(12)
# indeksi = np.random.permutation(len(xnew))
# indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))]
# indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)] 

# xtrain = xnew[indeksi_train,]
# ytrain = y_measured[indeksi_train]

# xtest = xnew[indeksi_test,]
# ytest = y_measured[indeksi_test]

# linearModel = lm.LinearRegression()
# linearModel.fit(xtrain,ytrain)

# ytest_p = linearModel.predict(xtest)
# MSE_test = mean_squared_error(ytest, ytest_p)

# plt.figure(1)
# plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
# plt.plot(xtest[:,1],ytest,'or',label='test')
# plt.legend(loc = 4) 

# #pozadinska funkcija vs model
# plt.figure(2)
# plt.plot(x,y_true,label='f')
# plt.plot(x, linearModel.predict(xnew),'r-',label='model')
# plt.xlabel('x')
# plt.ylabel('y')
# plt.plot(xtrain[:,1],ytrain,'ok',label='train')
# plt.legend(loc = 4)

# plt.show()


print('\n----- 5. Zadatak -------------------------------------------')

# def non_func(x):
#     y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
#     return y

# def add_noise(y):
#     np.random.seed(14)
#     varNoise = np.max(y) - np.min(y)
#     y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
#     return y_noisy
 
# x = np.linspace(1,10,50)
# y_true = non_func(x)
# y_measured = add_noise(y_true)

# x = x[:, np.newaxis]
# y_measured = y_measured[:, np.newaxis]

# degrees = [2, 6, 15]
# labels = ['Degree = 2', 'Degree = 6', 'Degree = 15']
# styles = ['r-', 'b-', 'y-']

# training_sample_sizes = [0.5, 0.7, 0.9]

# for training_sample_size in training_sample_sizes:
#     plt.figure()
#     MSEtrain = []
#     MSEtest = []
    
#     for i in range(3):
#         poly = PolynomialFeatures(degrees[i])
#         xnew = poly.fit_transform(x) 
        
#         np.random.seed(12)
#         indeksi = np.random.permutation(len(xnew))
#         indeksi_train = indeksi[0:int(np.floor(training_sample_size*len(xnew)))]
#         indeksi_test = indeksi[int(np.floor(training_sample_size*len(xnew)))+1:len(xnew)] 
        
#         xtrain = xnew[indeksi_train,]
#         ytrain = y_measured[indeksi_train]
        
#         xtest = xnew[indeksi_test,]
#         ytest = y_measured[indeksi_test]
        
#         linearModel = lm.LinearRegression()
#         linearModel.fit(xtrain,ytrain)
        
#         ytest_p = linearModel.predict(xtest)
#         ytrain_p = linearModel.predict(xtrain)
#         MSEtest.append(mean_squared_error(ytest, ytest_p))
#         MSEtrain.append(mean_squared_error(ytrain, ytrain_p))
        
#         plt.plot(x, linearModel.predict(xnew),styles[i],label=labels[i])

#     plt.plot(xtrain[:,1],ytrain,'ok',label='train')
#     plt.plot(x,y_true,label='f')
#     plt.xlabel('x')
#     plt.ylabel('y')
#     plt.legend(loc = 4) 
#     plt.title('Training sample size = ' + str(training_sample_size * 100) + '%')

#     print('Training sample size: ' + str(training_sample_size))
#     print('MSEtrain = ' + str(MSEtrain))
#     print('MSEtest = ' + str(MSEtest))
    
# plt.show() # više se poklapaju krivulje različitih stupnjeva s izvornom funkcijom

print('\n----- 6. Zadatak -------------------------------------------')

# def non_func(x):
#     y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
#     return y

# def add_noise(y):
#     np.random.seed(14)
#     varNoise = np.max(y) - np.min(y)
#     y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
#     return y_noisy

# x = np.linspace(1,10,100)
# y_true = non_func(x)
# y_measured = add_noise(y_true)

# x = x[:, np.newaxis]
# y_measured = y_measured[:, np.newaxis] 

# poly = PolynomialFeatures(15)
# xnew = poly.fit_transform(x) 

# np.random.seed(12)
# indeksi = np.random.permutation(len(xnew))
# indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))]
# indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)] 

# xtrain = xnew[indeksi_train,]
# ytrain = y_measured[indeksi_train]

# xtest = xnew[indeksi_test,]
# ytest = y_measured[indeksi_test]

# MSEtrain = []
# MSEtest = []
# lambdas = [0.1, 1, 10, 100, 1000]
# labels = ['Lambda = 0.1', 'Lambda = 1', 'Lambda = 10', 'Lambda = 100', 'Lambda = 1000']
# styles = ['r-', 'b-', 'y-', 'g-', 'k-']

# plt.figure()
# plt.plot(x,y_true,label='f')
# plt.xlabel('x')
# plt.ylabel('y')

# for i in range(5):
#     linearModel = lm.Ridge(lambdas[i])
#     linearModel.fit(xtrain, ytrain)
    
#     ytest_p = linearModel.predict(xtest)
#     ytrain_p = linearModel.predict(xtrain)
#     MSEtest.append(mean_squared_error(ytest, ytest_p))
#     MSEtrain.append(mean_squared_error(ytrain, ytrain_p))
    
#     plt.plot(x, linearModel.predict(xnew), styles[i], label=labels[i])

# plt.plot(xtrain[:,1], ytrain, 'ok', label='train')
# plt.legend(loc = 4) 

# print("MSEtrain: " + str(MSEtrain))
# print("MSEtest: " + str(MSEtest))

# plt.show() # Parametri rastu linearno povećanjem lambde

print('\n----- 7. Zadatak -------------------------------------------')

# boston = load_boston()
# x = boston.data
# y = boston.target

# print(boston.feature_names)

# cycol = cycle('bgrcmk')

# plt.figure()
# plt.suptitle('Ovisnost cijene kuće o parametrima dataseta')

# for i in range(6):
#     plt.subplot(2, 3, i+1)
#     if(i==3):
#         plt.scatter(np.where(x[:, i]==0), y[x[:, i]==0], c='r', label='Nije uz rijeku')
#         plt.scatter(np.where(x[:, i]==1), y[x[:, i]==1], c='g', label='Uz rijeku')
#         plt.legend()
#     elif i in [1,2,4]:
#         plt.scatter(range(len(y)), y, c = x[:, i], cmap = 'inferno')
#         plt.colorbar().set_label(boston.feature_names[i])
#         plt.xlabel('Indeks')
#     else:
#         plt.scatter(x[:,i], y, c=next(cycol))
#         plt.xlabel(boston.feature_names[i])
#     plt.ylabel('Cijena kuće')

# plt.figure() 
# plt.suptitle('Ovisnost cijene kuće o parametrima dataseta')
   
# for i in range(6, 12):
#     plt.subplot(2, 3, i+1-6)
#     if i in [8,9,10]:
#         plt.scatter(range(len(y)), y, c = x[:, i], cmap = 'inferno')
#         plt.colorbar().set_label(boston.feature_names[i])
#         plt.xlabel('Indeks')
#     else:
#         plt.scatter(x[:,i], y, c=next(cycol))
#         plt.xlabel(boston.feature_names[i])
#     plt.ylabel('Cijena kuće')
        
# plt.figure()
# plt.hist(y, bins=10)
# plt.title('Distribucija cijena kuća')
# plt.xlabel('Vrijednost kuće u tisućama $')

# lambdas = range(1000, 100001, 1000)

# #pronalazak optimalnog stupnja polinoma i regularizacijskog parametra
# for i in range(1, 6):
#     MSE_train = []
#     MSE_test = []

#     poly = PolynomialFeatures(degree=i)
#     x_t = poly.fit_transform(x)
    
#     np.random.seed(12)
#     indeksi = np.random.permutation(len(x_t))
#     indeksi_train = indeksi[0:int(np.floor(0.7*len(x_t)))]
#     indeksi_test = indeksi[int(np.floor(0.7*len(x_t)))+1:len(x_t)] 
    
#     xtrain = x_t[indeksi_train]
#     ytrain = y[indeksi_train]
    
#     xtest = x_t[indeksi_test]
#     ytest = y[indeksi_test] 
    
#     for lmbd in lambdas:
#         linearModel = lm.Ridge(lmbd)
#         linearModel.fit(xtrain,ytrain)
        
#         ytrain_p = linearModel.predict(xtrain)
#         MSE_train.append(mean_squared_error(ytrain, ytrain_p))
    
#         ytest_p = linearModel.predict(xtest)
#         MSE_test.append(mean_squared_error(ytest, ytest_p))

#     plt.figure()
#     plt.xlabel('Regularizacijski parametar')
#     plt.ylabel('Srednja kvadratna pogreška')
#     plt.plot(lambdas, MSE_train, label = 'Train')
#     plt.plot(lambdas, MSE_test, label = 'Test')
#     plt.title('Vrijednosti pogrešaka za stupanj polinoma ' + str(i))
#     plt.legend()

# #treniranje i prikaz rezultata za stupanj polinoma 2 i vrijednost regularizacijskog parametra 3000
# poly = PolynomialFeatures(degree=2)
# x_t = poly.fit_transform(x)

# np.random.seed(12)
# indeksi = np.random.permutation(len(x_t))
# indeksi_train = indeksi[0:int(np.floor(0.7*len(x_t)))]
# indeksi_test = indeksi[int(np.floor(0.7*len(x_t)))+1:len(x_t)] 

# xtrain = x_t[indeksi_train]
# ytrain = y[indeksi_train]

# xtest = x_t[indeksi_test]
# ytest = y[indeksi_test] 

# linearModel = lm.Ridge(3000)
# linearModel.fit(xtrain,ytrain)

# ytrain_p = linearModel.predict(xtrain)

# ytest_p = linearModel.predict(xtest)
# print('MSE = ' + str(mean_squared_error(ytest, ytest_p)))

# plt.figure()
# plt.ylim((0,60))
# plt.scatter(range(len(ytest)), ytest[ytest.argsort()], c = 'g', label = 'Stvarna cijena')
# plt.scatter(range(len(ytest)), ytest_p[ytest.argsort()], c = 'r', label = 'Predviđena cijena')
# plt.vlines(range(len(ytest)), ytest[ytest.argsort()], ytest_p[ytest.argsort()])
# plt.xlabel('Redni broj kuće')
# plt.ylabel('Vrijednost kuće u tisućama $')
# plt.legend()

