Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value).

Ispravci:
- dodane su zagrade funkciji print
- raw_input -> input (raw_input je deprecated u python3)
- fnamex -> fname
- zakomentirana linija 13
- '+=' u liniji 18 umjesto '='