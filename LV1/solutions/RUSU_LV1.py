#------------------------------------
#------------------------------------
# 1. zadatak
#------------------------------------

# radniSati = input("Unesi broj radnih sati: ")
# knPoSatu = input("Unesi kn po satu: ")
# ukupno = float(radniSati) * float(knPoSatu)
# print('Ukupno:' , ukupno)


#------------------------------------
#------------------------------------
# 2. zadatak
#------------------------------------

# ocjena = input("Unesi ocjenu: ")
# try:
#     ocjena = float(ocjena)
#     if (ocjena < 1.0 and ocjena >= 0.9):
#         print("A")
#     elif (ocjena < 0.9 and ocjena >= 0.8):
#         print("B")       
#     elif (ocjena < 0.8 and ocjena >= 0.7):
#         print("C")
#     elif (ocjena < 0.7 and ocjena >= 0.6):
#         print("D")
#     elif (ocjena < 0.6 and ocjena >= 0.0):
#         print("F")
#     else: 
#         print("Ocjena je izvan granica!")
# except ValueError:
#     print("Upišite brojčanu vrijednost")


#------------------------------------
#------------------------------------
# 3. zadatak
#------------------------------------

# def total_kn(radniSati, knPoSatu):
#     ukupno = float(radniSati) * float(knPoSatu)
#     return ukupno
    
# radniSati = input("Unesi broj radnih sati: ")
# knPoSatu = input("Unesi kn po satu: ")
# #ukupno = float(radniSati) * float(knPoSatu)
# print('Ukupno:' , total_kn(radniSati, knPoSatu))


#------------------------------------
#------------------------------------
# 4. zadatak
#------------------------------------

# unos = ''
# sum = 0.0
# count = 0
# minNumber = float('inf')
# maxNumber = float('-inf')
# while unos != 'Done':
#     unos=input("Unesi brojeve: ")
#     try:
#         val = int(unos)
#         sum = sum + float(unos)
#         count = count + 1
#         minNumber = min(float(unos), minNumber)
#         maxNumber = max(float(unos), maxNumber)
#     except ValueError:
#         if unos != 'Done':
#             print("Niste upisali broj!")

# print("Broj upisanih brojeva: ", count)
# if(count!=0):
#     print("Average: ", sum/float(count))
# print("Minimalni broj: ", minNumber)
# print("Maksimalni broj: ", maxNumber)


#------------------------------------
#------------------------------------
# 5. zadatak 
#------------------------------------

# results=[]
# suma = 0
# unos = input("Unesi naziv txt: ")
# try:
#     file=open(unos,"r")
#     index = 0
#     for line in file:
#         index += 1
#         if "X-DSPAM-Confidence: " in line:
#             results.append(line.rstrip())
#     i=0
#     size=len(results)
#     while i < size:
#         suma += float(results[i].split(":",1)[1])
#         i+=1
#     average = suma/i
#     print(average)
 
# except FileNotFoundError:
#     print("Datoteka ne postoji")


#------------------------------------
#------------------------------------
# 6. zadatak
#------------------------------------

# import re
 
# lst = []
# brojac = 0
# hostname = []
# fhand = open('../resources/mbox-short.txt')
# for line in fhand:
#     line = line.rstrip()
#     if(line.startswith('From:')):
#         mail = re.findall('\S*@\S+', line)
#         lst.append(mail[0])
#         brojac += 1
# i = 0
# while i < brojac:
#     email = lst[i]
#     res = email[email.index('@') + 1 : ]
#     hostname.append(res)
#     i += 1
   
# i = 0
# while i < brojac:
#     print(lst[i])
#     print(hostname[i])
#     i+=1
   
# dictionary = {}

# for x in hostname:
#     if x in dictionary:
#         dictionary[x] += 1
#     else:
#         dictionary[x] = 1
       
# for key, value in dictionary.items():
#     print(key, value)
