# -*- coding: utf-8 -*-

from tensorflow.keras.preprocessing import image_dataset_from_directory
import pandas as pd
from tensorflow import keras
from tensorflow.keras import layers
import os
import shutil

train_ds = image_dataset_from_directory(directory='archive/Train/',
                                        labels='inferred',
                                        label_mode='categorical',
                                        batch_size=32,
                                        image_size=(48, 48))


test_ds = image_dataset_from_directory(directory='archive/Test_dir/',
                                        labels='inferred',
                                        label_mode='categorical',
                                        batch_size=32,
                                        image_size=(48, 48))

model = keras.Sequential()
model.add(keras.Input(shape=(48,48,3)))
model.add(layers.Conv2D(32, kernel_size=(3,3),padding='same', activation="relu"))
model.add(layers.Conv2D(32, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(64, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(64, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(128, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(128, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Flatten())
model.add(layers.Dense(units=512, activation='relu'))
model.add(layers.Dropout(rate=0.5))
model.add(layers.Dense(units=43, activation='softmax'))
model.summary()

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# TODO: provedi ucenje mreze
#fit
model.fit_generator(train_ds, epochs=5)

#spremanje modela
model.save("CNN/")

# stvaranje direktorija za sortiranje test podataka
path=os.path.abspath("archive/Test_dir")
os.mkdir(path)
for i in range(0,43):
    test_dir=str(i)
    parent_dir = os.path.abspath("archive/Test_dir")
    path=os.path.join(parent_dir,test_dir)
    print(path)
    os.mkdir(path)
    
# raspored u posebne mape
test_data=pd.read_csv('archive/Test.csv')
print(test_data)
print(test_data["Path"][0])

for i in range (0,len(test_data)):
    source="archive/"+str(test_data["Path"][i])
    #print(source)
    num=str(test_data["ClassId"][i])
    destination="archive/Test_dir/"+num
    dest = shutil.copy2(source, destination) 

model = keras.models.load_model("CNN/")

score = model.evaluate(test_ds)
print("Test loss:", score[0])
print("Test accuracy:", score[1])