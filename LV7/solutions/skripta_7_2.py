from tensorflow import keras
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from matplotlib import pyplot as plt
import sklearn
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
import numpy as np

filename = 'images/test.png'

img = mpimg.imread(filename)
img = color.rgb2gray(img)
img = resize(img, (28, 28))


plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()


img = img.reshape(1, 28, 28, 1)
img = img.astype('float32')


# TODO: ucitaj model
model = keras.models.load_model("CNN/")

# TODO: napravi predikciju 
p=model.predict(img)
print(p[0])
max=np.max(p)
print(max)

for i in range (len(p[0])):

  if(max==p[0][i]):
    max_i=i


# TODO: ispis rezultat
print("------------------------")
print(max_i)



