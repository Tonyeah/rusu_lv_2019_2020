import re
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg
import pandas as pd 
 
#   ----------
#   1. Zadatak
#   ----------

# file=open('../resources/mbox-short.txt')
# firstPart = []
# for line in file:
#     email=re.findall('\S+@\S+', line)
#     if(email):
#         index = email[0].index('@')
#         if(email[0][0]=='<'):
#             firstPart.append(email[0][1:index])
#         else:
#             firstPart.append(email[0][:index])
           
# print(firstPart)


#   ----------
#   2. Zadatak
#   ----------

# file = open('../resources/mbox-short.txt')
# atLeast1A=[]
# exactly1A=[]
# noA=[]
# numbers=[]
# nonCapitalLetters=[]
# for line in file:
#     email = re.findall('\S+@\S+', line)
#     if(email):
#         index = email[0].index('@')
#         if(email[0][0]=='<'):
#             name=email[0][1:index]
#         else:
#             name=email[0][:index]
#         if(name):
#             if name.count('a') >= 1:
#                 atLeast1A.append(name)
#             if name.count('a') == 1:
#                 exactly1A.append(name)
#             if re.match('[^a]*', name):
#                 noA.append(name)
#             if re.match('.*[0-9]+.*', name):
#                 numbers.append(name)
#             if re.match('^[a-z]+$',name):
#                 nonCapitalLetters.append(name)
# # print(atLeast1A)
# # print(exactly1A)
# # print(noA)
# # print(numbers)
# # print(nonCapitalLetters)
    

#   ----------
#   3. Zadatak
#   ----------

# def GetAverageHeights():
#     heightSum_male = np.zeros([])
#     np.dot(gender, height_total, heightSum_male)
#     heightSum_female = height_total.sum() - heightSum_male
#     avgHeight_male = heightSum_male/gender.sum()
#     avgHeight_female = heightSum_female/(n - gender.sum())
#     return [avgHeight_male, avgHeight_female]

# n = 50
# j=0
# k=0
# gender = np.random.randint(0,2, size = n)
# height_total =np.zeros(n)
# height_male = []
# height_female = []

# for i in range(n):
#     if gender[i] == 1:
#         height_total[i] = np.random.normal(180, 7)
#     else:
#         height_total[i] = np.random.normal(167, 7)
       
# color = np.array(['r','b'])
# plt.scatter(gender, height_total, marker = '.', c = color[gender])
# averageHeights = GetAverageHeights()
# genders = np.array([1, 0])
# plt.scatter(genders, averageHeights, marker = 'x', c = 'black')

#   ----------
#   4. Zadatak
#   ----------

# np.random.seed(55)
# bacanja = np.random.randint(1, 7, size=100)
# plt.hist(bacanja, density=True, bins=30)

#   ----------
#   5. Zadatak
#   ----------


# fig = plt.figure()
# ax = fig.add_subplot(1,1,1)

# mtcars = pd.read_csv("../resources/mtcars.csv")
# print(mtcars)
# plt.scatter(mtcars.mpg, mtcars.hp) 
# plt.xlabel("mpg")                   
# plt.ylabel("hp")                    
# plt.grid(linestyle='--')            

# for i in range (0, len(mtcars.mpg)):       
#     ax.annotate('%.2f' % mtcars.wt[i],      
#                 xy=(mtcars.mpg[i], mtcars.hp[i]), 
#                 xytext=(mtcars.mpg[i]+2, mtcars.hp[i]+20), 
#                 arrowprops=dict(arrowstyle="-", facecolor='black'),
#                 )
    
# maxmpg=0.0
# minmpg=9999.0
# avg=0
# for i in range (0, len(mtcars.mpg)):
#     if(mtcars.mpg[i]>maxmpg):
#         maxmpg = mtcars.mpg[i]
#     if(mtcars.mpg[i]<minmpg):
#         minmpg = mtcars.mpg[i]
#     avg += mtcars.mpg[i]
# avg/=len(mtcars.mpg)
# print("Minimalna potrosnja: ", minmpg) 
# print("Maksimalna potrosnja: ", maxmpg) 
# print("Srednja vrijednost potrosnje: ", avg)

#   ----------
#   6. Zadatak
#   ----------

# img = mpimg.imread('../resources/tiger.png') 
# imgplot = plt.imshow(img)
# img *= 2
# imgplot = plt.imshow(img)
